//
//  MainViewController.swift
//  testApp
//
//  Created by Nikolay on 14.04.21.
//

import UIKit
import Charts

enum Meals {
    case breakfast
    case lunch
    case dinner
}

class MainViewController: UIViewController {
    // MARK: - IBOutlet
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var viewBreakeast: UIView!
    @IBOutlet weak var kcalLabel: UILabel!
    @IBOutlet weak var viewLineChart: LineChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var burntLabel: UILabel!
    
    // MARK: - Variable
    private var array: [ModelForCollectionView] = [
        ModelForCollectionView(name: "BREAKFAST", kcal: "", time: ""),
        ModelForCollectionView(name: "LUNCH", kcal: "", time: ""),
        ModelForCollectionView(name: "DINNER", kcal: "", time: ""),
        ModelForCollectionView(name: "", kcal: "", time: "")
    ]
    private var yValues: [ChartDataEntry] = [
        ChartDataEntry(x: 0, y: 0),
        ChartDataEntry(x: 0.5, y: 0),
        ChartDataEntry(x: 1.65, y: 0),
        ChartDataEntry(x: 2.65, y: 0),
        ChartDataEntry(x: 4, y: 0)
    ]
    private var eating = 0
    private var burnt = 90
    private var width: CGFloat = 4.6
    
    // MARK: - Lifecycle function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupChartView()
        collectionView.register(UINib(nibName: CollectionViewCell.collectionCellIdentifier, bundle: nil), forCellWithReuseIdentifier: CollectionViewCell.collectionCellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewLineChart.alpha = 0
        collectionView.reloadData()
        view.getGradient(view: view)
        self.burntLabel.text = "\(burnt) kcal"
    }
}

// MARK: - Private methods
private extension MainViewController {
    func setupChartView() {
        viewLineChart.leftAxis.enabled = false
        viewLineChart.rightAxis.enabled = false
        viewLineChart.xAxis.enabled = false
        viewLineChart.animate(yAxisDuration: 2)
        setData()
    }
    
    func showAlert(title: String, message: String, complition: @escaping (UIAlertAction) -> Void) {
    let alertController = UIAlertController(title: "Введите сьеденое количество каллорий за \(title)", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: complition)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getAlert(labelKcal: UILabel,timeLabel: UILabel, complition: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: "Введите сьеденое количество каллорий за \(Meals.breakfast)", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
            let textField = alertController.textFields?.first
            if let task = textField?.text {
                self.eating += Int(task) ?? Int()
                var count = Int(task) ?? Int()
                count += Int(labelKcal.text ?? String()) ?? Int()
                labelKcal.text = "\(count)"
                self.getDate(label: timeLabel)
                complition(count)
                UIView.animate(withDuration: 2) {
                    self.viewLineChart.alpha = 1
                }
                self.setupChartView()
                self.kcalLabel.text = "\(self.eating) kcal"
                self.totalLabel.text = "\(self.eating - self.burnt)"
            }
        }
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setData() {
        let set1 = LineChartDataSet(entries: yValues)
        set1.mode = .cubicBezier
        set1.lineWidth = 3
        set1.setColor(UIColor(red: 0.58, green: 0.961, blue: 0.961, alpha: 1))
        set1.circleRadius = 3
        set1.circleColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        let data = LineChartData(dataSet: set1)
        viewLineChart.data = data
    }
    
    func getDate(label: UILabel) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        let result = formatter.string(from: date)
        label.text = result
    }
}

// MARK: - UICollectionViewDataSource
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        let objects = array[indexPath.item]
        cell.update(with: objects)
        cell.yourobj = {
            cell.setLayer(view: cell.myView)
            cell.myView.getGradient(view: cell.myView)
            self.getAlert(labelKcal: cell.kcalLabel, timeLabel: cell.timeLabel) { [weak self] (count) in
                guard let self = self else { return }
                switch objects.name {
                case "BREAKFAST":
                    self.yValues[1] = ChartDataEntry(x: 0.5, y: Double(count))
                case "LUNCH":
                    self.yValues[2] = ChartDataEntry(x: 1.65, y: Double(count))
                case "DINNER":
                    self.yValues[3] = ChartDataEntry(x: 2.65, y: Double(count))
                default:
                    break
                }
            }
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameCV = collectionView.frame
        let widthCell = frameCV.width / width
        return CGSize(width: widthCell, height: collectionView.frame.height)
    }
}



