//
//  CollectionViewCell.swift
//  testApp
//
//  Created by Nikolay on 14.04.21.
//

import UIKit

protocol IModelCollectionViewCell {
    func update(with object: ModelForCollectionView?)
}

class CollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var kcalLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var myView: UIView!
    
    // MARK: - Variable
    var yourobj:(() -> Void)? = nil
    static let collectionCellIdentifier = "CollectionViewCell"
    
    // MARK: - Lifecycle function
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        myView.layer.cornerRadius = 11
        myView.layer.borderWidth = 0.2
        myView.layer.borderColor = UIColor(red: 0.342, green: 0.339, blue: 0.339, alpha: 0.28).cgColor
        myView.backgroundColor = #colorLiteral(red: 0.9699861407, green: 0.960067451, blue: 0.9776807427, alpha: 1)
    }
    
    @IBAction func buttonAddAction(_ sender: UIButton) {
        if let buttonAddAction = self.yourobj {
            buttonAddAction()
        }
    }
}

extension CollectionViewCell: IModelCollectionViewCell {
    func update(with object: ModelForCollectionView?) {
        guard let object = object else { return }
        nameLabel.text = object.name
        kcalLabel.text = object.kcal
        timeLabel.text = object.time
    }
    
    
}
