//
//  ModelForCollectionView.swift
//  testApp
//
//  Created by Nikolay on 14.04.21.
//

import Foundation

struct ModelForCollectionView {
    var name: String?
    var kcal: String?
    var time: String?
}
