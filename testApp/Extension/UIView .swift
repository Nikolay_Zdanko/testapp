//
//  UIView .swift
//  testApp
//
//  Created by Nikolay on 14.04.21.
//

import Foundation
import UIKit
extension UIView {
    
    func getGradient(view: UIView) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [
            UIColor(red: 0.933, green: 0.929, blue: 0.945, alpha: 1).cgColor,
            UIColor(red: 0.96, green: 0.955, blue: 0.975, alpha: 1).cgColor
        ]
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer.position = view.center
        self.layer.insertSublayer(layer, at: 0)
        
        let layer2 = CAGradientLayer()
        layer2.frame = self.bounds
        layer2.colors = [
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
        
        layer2.locations = [0, 1]
        layer2.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer2.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer2.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer2.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer2.position = view.center
        layer2.cornerRadius = 13
        self.layer.insertSublayer(layer2, at: 1)
    }
    
    //shadow
        func setLayer(view: UIView) {
//            view.layer.cornerRadius = 11
            view.layer.masksToBounds = true
            
            self.layer.shadowOpacity = 1
            self.layer.shadowOffset = CGSize(width: 10, height: 20)
            self.layer.shadowRadius = 30
            self.layer.shadowColor = UIColor(red: 0.802, green: 0.802, blue: 0.881, alpha: 0.54).cgColor
            self.layer.masksToBounds = false
        }
    
}
